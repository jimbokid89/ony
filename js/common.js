'use strict';
if (!window.console) window.console = {};
if (!window.console.memory) window.console.memory = function() {};
if (!window.console.debug) window.console.debug = function() {};
if (!window.console.error) window.console.error = function() {};
if (!window.console.info) window.console.info = function() {};
if (!window.console.log) window.console.log = function() {};

var isMac = navigator.platform.toUpperCase().indexOf('MAC') >= 0;
var ios = (/iphone|ipad|ipod/i.test(navigator.userAgent.toLowerCase()));
var isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

if (isFirefox) {
    $('html').addClass('firefox');
}

var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0

if (isSafari) {
    $('ti')
}

if (isMac) {
    $('html').addClass('macOs');
}

if (ios) {
    $('html').addClass('ios');
}

// sticky footer
//-----------------------------------------------------------------------------
if (!Modernizr.flexbox) {
    (function() {
        var
            $pageWrapper = $('#page-wrapper'),
            $pageBody = $('#page-body'),
            noFlexboxStickyFooter = function() {
                $pageBody.height('auto');
                if ($pageBody.height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
                    $pageBody.height($(window).height() - $('#header').outerHeight() - $('#footer').outerHeight());
                } else {
                    $pageWrapper.height('auto');
                }
            };
        $(window).on('load resize', noFlexboxStickyFooter);
    })();
}
if (ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
    (function() {
        var
            $pageWrapper = $('#page-wrapper'),
            $pageBody = $('#page-body'),
            ieFlexboxFix = function() {
                if ($pageBody.addClass('flex-none').height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
                    $pageWrapper.height($(window).height());
                    $pageBody.removeClass('flex-none');
                } else {
                    $pageWrapper.height('auto');
                }
            };
        ieFlexboxFix();
        $(window).on('load resize', ieFlexboxFix);
    })();
}

if (ieDetector.ieVersion != false) { // if IE
    $('body').on("mousewheel", function() {
        // remove default behavior
        // console.log(event);
        event.preventDefault();
        //scroll without smoothing
        var wheelDelta = event.wheelDelta;
        var currentScrollPosition = window.pageYOffset;
        window.scrollTo(0, currentScrollPosition - wheelDelta, 500, {
            easing: 'easeInOutCirc'
        });
    });
}

var pageState = 'index';
var videoLoaded = false;
$(function() {
    // placeholder
    //-----------------------------------------------------------------------------
    $('input[placeholder], textarea[placeholder]').placeholder();
    //Lets do some global variables
    var mobile = $(window).outerWidth() < 767;
    if (!(mobile)) {
        var videoNames = ['BURO', 'MAIN', 'Perebivka_1', 'Perebivka_2', 'Perebivka_3', 'Perebivka_4', 'Perebivka_5', 'Perebivka_6', 'Perebivka_7', 'Perebivka_8', 'PROJECTS']
        videoNames.forEach(function(name) {
            if (name.match('Perebivka') == null) {
                var className = name;
            } else {
                var className = 'Perebivka';
            }
            $('.loader').append('<video class="' + className + '" preload="auto"><source src="video/' + name + '.mp4" type="video/mp4"></video>');
        });
        $(window).load(function() {
            videoLoaded = true;
        })
    }

    $('body').append('<div class="save-overlay"></div>')
    var saveOverlay = $('.save-overlay');
    //Bureau page begin
    function bureauFire() {
        var teamList = $('.bureau__team-list li');
        var originalLength = (teamList.length) - 1;
        var listCount = 0;
        if ($(window).outerWidth() > 767) {
            $('.js-target-list').addClass('new-list');
        } else {
            $('.bureau__team-list').append('<ul class="mobile-team-list"></ul>');
            $('.bureau__team-list li').each(function(key) {
                var targetLi = $(this);
                targetLi.clone().appendTo('.mobile-team-list');
                targetLi.remove();
                if (key == originalLength) {
                    $('.js-target-list').remove();
                }
            });

            $('.bureau__slider-item').css({
                'background-position': 'right'
            })

            $('.mobile-team-list').on('init', function() {
                $('.bureau__team > .container').append('<div class="team-count"><span class="current-slide-num">1</span>/<span>' + $('.mobile-team-list li').length + '</span></div>');
            });

            $('.mobile-team-list').on('afterChange', function(nextSlide) {
                var numberSlideTeam = parseInt($('.bureau__team-list .slick-active').attr('data-slick-index')) + 1;
                $('.current-slide-num').html(numberSlideTeam);
            });

            $('.mobile-team-list').slick({
                infinite: false
            });
        }

        function sliderBg() {
            // console.log('slider size');
            $('.bureau__slider-img').load(function() {
                var sliderImg = $('.bureau__slider-img'),
                    theWindow = $(window),
                    aspectRatio = sliderImg.width() / sliderImg.height();
                if (($('.bureau__img-wrapper').width() / $('.bureau__img-wrapper').height()) < aspectRatio) {
                    sliderImg.removeClass('bgwidth').addClass('bgheight');
                } else {
                    sliderImg.removeClass('bgheight').addClass('bgwidth');
                }
            })
            $('.js-scroll-down').on('click', function(e) {
                scrollToContent();
            });
        }
        sliderBg()
        window.addEventListener('resize', sliderBg);
        $('.bureau__slider-item').eq(1).find('.bureau__img-wrapper').css({
            'right': 0,
            'left': 'auto',
        })
        $(document).on('mouseover', '.js-move-slide', function() {
            var _this = $(this),
                slideTarget = _this.attr('data-slide');
            if (slideTarget == 0) {
                $('.bureau__img-wrapper').css({
                    'transform': 'translateX(50px)',
                    'transition': '1s ease'
                })
            } else if (slideTarget == 1) {
                $('.bureau__img-wrapper').css({
                    'transform': 'translateX(-50px)',
                    'transition': '1s ease'
                })
            }
        });

        $(document).on('mouseout', '.js-move-slide', function() {
            $('.bureau__img-wrapper').css({
                'transform': 'translateX(0px)',
                'transition': '1s ease'
            })
        })

        $(document).on('click', '.js-show-slide', function(e) {
            e.preventDefault();
            var _this = $(this),
                slideTarget = _this.attr('data-slide');
            if (slideTarget == 1) {
                showRight(slideTarget);
            } else if (slideTarget == 0) {
                showLeft(slideTarget)
            }

            $('.bureau__img-wrapper').css({
                'transform': 'translateX(0px)',
                'transition': '1s ease'
            })

        })

        function showRight(slideTarget) {
            var slider = $('.bureau__slider');
            slider.find('.bureau__slider-item').removeClass('active right left');
            $('.bureau__slider-item').eq(slideTarget).addClass('active');
            setTimeout(function() {
                $('.moving').removeClass('moving');
                $('.bureau__slider-item').eq(0).addClass('left moving').css({
                    'z-index': 3
                });
                setTimeout(function() {
                    $('.bureau__slider-item').eq(0).css({
                        'z-index': 3
                    });
                    $('.bureau__slider-item').eq(slideTarget).addClass('active').css({
                        'z-index': 2
                    });
                }, 800)
            }, 800);
            $('.js-dots').removeClass('active');
            $('.js-dots').eq(1).addClass('active');
        }

        function showLeft(slideTarget) {
            var slider = $('.bureau__slider');
            slider.find('.bureau__slider-item').removeClass('active right left');
            $('.bureau__slider-item').eq(slideTarget).addClass('active');
            setTimeout(function() {
                $('.moving').removeClass('moving');
                $('.bureau__slider-item').eq(1).addClass('right moving');
                setTimeout(function() {
                    $('.bureau__slider-item').eq(1).css({
                        'z-index': 3
                    });
                    $('.bureau__slider-item').eq(slideTarget).addClass('active').css({
                        'z-index': 2
                    });
                }, 800)
            }, 800);
            $('.js-dots').removeClass('active');
            $('.js-dots').eq(0).addClass('active');
        }

        var xPos = 0;
        var distance = 0;
        $(document).on('touchstart', '.js-bureau-slider', function(e) {
            xPos = e.originalEvent.touches[0].pageX;
        });

        $(document).on('touchend', '.js-bureau-slider', function(e) {
            var xPosend = e.originalEvent.changedTouches[0].pageX;
            distance = xPos - xPosend;

            if (distance > 120) {
                if ($('.js-dots.active').parent('li').index() == 1) {
                    return
                }
                showRight(1);
            } else if (distance < -120) {
                if ($('.js-dots.active').parent('li').index() == 0) {
                    return
                }
                showLeft(0)
            }
        });

        $(document).on('click', '.js-dots', function(e) {
            var slider = $('.bureau__slider');
            e.preventDefault();
            var _this = $(this);
            if ($(this).hasClass('active')) {
                return;
            }
            var slideTarget = _this.attr('data-slide');
            $('.js-dots.active').removeClass('active');
            _this.addClass('active');
            if (slideTarget == 1) {
                showRight(slideTarget);
            } else if (slideTarget == 0) {
                showLeft(slideTarget)
            }
            $('.bureau__img-wrapper').css({
                'transform': 'translateX(0px)',
                'transition': '1s ease'
            })
        });
        //***********************************************************
        $('.new-list .bureau__team-person').mouseover(function() {
            if (!(mobile)) {
                $('.team-img-hover').attr('src', $(this).closest('.new-list').find('img').attr('src')).css('opacity', 1);
            }
        });
        $('.new-list .bureau__team-person').mouseout(function() {
            $('.team-img-hover').css('opacity', 0);
        });
        //***********************************************************
        var oldClientHeight = 0;
        if (mobile) {
            $('.clients__list li').each(function(key) {
                if (key === parseInt($('.clients__list').attr('data-show-more'))) {
                    return false
                }
                oldClientHeight += $(this).height();
            })
            $('.clients__wrapper').css({
                'height': oldClientHeight
            })
        }

        $('.js-show-clients').on('click', function(e) {
            e.preventDefault();
            var targetHeight = $('.clients__wrapper .clients__list').outerHeight();
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('.clients__wrapper').css({
                    'height': oldClientHeight
                })
            } else {
                $(this).addClass('active');
                $('.clients__wrapper').css({
                    'height': targetHeight
                })
            }
        });
    }

    function initMap() {
        var map;
        var myLatlng = new google.maps.LatLng(55.762553, 37.620266);

        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(55.762553, 37.620266),
            zoom: 19,
            scrollwheel: false,
        });
        var image = new google.maps.MarkerImage('img/map-icon.png',
            // /local/templates/main/
            new google.maps.Size(67, 81)
        );
        var marker = new google.maps.Marker({
            icon: image,
            position: myLatlng,
        });
        marker.setMap(map);
        map.setOptions({
            styles: styles
        });
    };

    var styles = [{
        "featureType": "landscape",
        "stylers": [{
            "saturation": -100
        }, {
            "lightness": 65
        }, {
            "visibility": "on"
        }]
    }, {
        "featureType": "poi",
        "stylers": [{
            "saturation": -100
        }, {
            "lightness": 51
        }, {
            "visibility": "simplified"
        }]
    }, {
        "featureType": "road.highway",
        "stylers": [{
            "saturation": -100
        }, {
            "visibility": "simplified"
        }]
    }, {
        "featureType": "road.arterial",
        "stylers": [{
            "saturation": -100
        }, {
            "lightness": 30
        }, {
            "visibility": "on"
        }]
    }, {
        "featureType": "road.local",
        "stylers": [{
            "saturation": -100
        }, {
            "lightness": 40
        }, {
            "visibility": "on"
        }]
    }, {
        "featureType": "transit",
        "stylers": [{
            "saturation": -100
        }, {
            "visibility": "simplified"
        }]
    }, {
        "featureType": "administrative.province",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "water",
        "elementType": "labels",
        "stylers": [{
            "visibility": "on"
        }, {
            "lightness": -25
        }, {
            "saturation": -100
        }]
    }, {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [{
            "hue": "#000"
        }, {
            "lightness": -25
        }, {
            "saturation": 100
        }]
    }, {
        "elementType": "geometry.stroke",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#e1d9d7"
        }]
    }];

    //***************************************************************************
    function aspectRatioHeaderBg() {
        var headerImg = $('.header__background');
        var theWindow = $(window),
            $bg = headerImg,
            aspectRatio = $bg.width() / $bg.height();
        function resizeBg() {
            if (($('.header').outerWidth() / $('.header').outerHeight()) < ($('.header__background').width() / $('.header__background').height())) {
                $bg.removeClass('bgwidth').addClass('bgheight');
            } else {
                $bg.removeClass('bgheight').addClass('bgwidth');
            }
        }
        theWindow.resize(resizeBg).trigger("resize");
    }

    function aspectRatioProjectHeader() {
        var headerImg = $('.header__background, .next-project img');
        var theWindow = $(window),
            $bg = headerImg,
            aspectRatio = $bg.width() / $bg.height();
        function resizeBg() {
            setTimeout(function() {
                if (($('.header__wrap').outerWidth() / $('.header__wrap').outerHeight()) < ($('.header__background').outerWidth() / $('.header__background').outerHeight())) {
                    $('.header-inner .header__background').removeClass('bgwidth').addClass('bgheight');
                } else {
                    $('.header-inner .header__background').removeClass('bgheight').addClass('bgwidth');
                }


                if (($('.next-project .img-wrapper').outerWidth() / $('.next-project .img-wrapper').outerHeight()) < ($('.next-project img').outerWidth() / $('.next-project img').outerHeight())) {
                    $('.next-project img').removeClass('bgwidth').addClass('bgheight');
                } else {
                    $('.next-project img').removeClass('bgheight').addClass('bgwidth');
                }
            }, 10)
        }
        theWindow.resize(resizeBg).trigger("resize");
    }

    aspectRatioHeaderBg();
    var delay = 0.2;
    var imgWrapper = $('.next-project .img-wrapper');
    var theWindow = $(window),
        $bg = imgWrapper,
        aspectRatio = $bg.find('img').width() / $bg.find('img').height();
    if (($('.next-project').width() / $('.next-project').height()) < aspectRatio) {
        $bg.find('img').addClass('bgheight');
    } else {
        $bg.find('img').addClass('bgwidth');
    }

    $(document).on('click', '.js-scroll-down', function(e) {
        e.preventDefault();
        scrollToContent();
    });

    function scrollToContent() {
        if (mobile) {
            $('html, body').animate({
                scrollTop: $('.info-wrapper').offset().top
            }, 300);
        } else {

            $('html, body').animate({
                scrollTop: $('.info-wrapper').offset().top
            }, 1000, 'easeInQuart');
        }
    }

    function bindInView() {
        $('.js-inview').on('inview', function(event, isInView) {
            if (isInView) {
                $(this).addClass('animation');
            }
        });
    }

    bindInView();
    var sliderWidth = 0;
    function initProjectInnerSlider() {
        $('.js-project-slide').on('mouseover', function() {
            var targetPosition = sliderWidth - $(window).outerWidth();
            // console.log(targetPosition);
            switch ($(this).attr('data-direction')) {
                case 'right':
                    $('.images__slider .slick-track').css({
                        'left': '-20px',
                        'transition': 'left 0.5s',
                    })
                    break;
                case 'left':
                    $('.images__slider .slick-track').css({
                        'left': '20px',
                        'transition': 'left 0.5s',
                    })
                    break;
                default:
            }
        });

        $('.js-project-slide').on('mouseout', function() {
            $('.images__slider .slick-track').css({
                'left': '0px',
                'transition': 'left 0.5s ease-in-out',
            })
        });
    };

    initProjectInnerSlider();
    var openProj = false;
    $(document).on('click', '.project-main__item', function(e) {
        e.preventDefault();
        // firstVisit = true;
        var _this = $(this);
        $('.logo--small').fadeOut();
        openProj = true;
        $('.header__left-btn,.header__right-btn').css({
            'z-index': '0',
        })
        $('.header__menu').fadeOut();
        _this.find('.img-wrapper').clone().appendTo('.header-target');
        var newImage = $('.header-target .img-wrapper');
        if (isFirefox || (ieDetector.ieVersion != false)) {
            $('.header-target').css({
                'opacity': 0,
                'transition': 'opacity 0'
            })
        }
        if (!(mobile)) {
            newImage.addClass('masked');
        }

        var headerImg = newImage.find('img').attr('src');
        var theWindow = $(window),
            aspectRatio = newImage.find('img').outerWidth() / newImage.find('img').outerHeight();


        if (!(mobile)) {
            if ((theWindow.width() / theWindow.height()) < (newImage.find('img').outerWidth() / newImage.find('img').outerHeight())) {
                newImage.find('img').removeClass('bgwidth').addClass('bgheight');
            } else {
                newImage.find('img').removeClass('bgheight').addClass('bgwidth');
            }
        } else {
            // debugger
            setTimeout(function() {
                if ((theWindow.outerWidth() / 415) < (newImage.find('img').outerWidth() / newImage.find('img').outerHeight())) {
                    newImage.find('img').removeClass('bgwidth').addClass('bgheight');
                } else {
                    newImage.find('img').removeClass('bgheight').addClass('bgwidth');
                }

            }, 100)

        }
        $('.logo--small').css({
            'opacity': 0
        })
        if (mobile) {
            newImagePos = {
                left: $(this).offset().left,
                top: ($(this).offset().top - $(window).scrollTop()),
                originWidth: $(this).outerWidth(),
                originHeight: $(this).outerHeight(),
                scrollPosition: $(window).scrollTop(),
                offsetTop: $(this).offset().top
            };

            newImage.css({
                'position': 'fixed',
                'z-index': 20,
                'top': newImagePos.top + 'px',
                'left': newImagePos.left + 'px',
                'width': newImagePos.width + 'px',
                'height': newImagePos.height + 'px',
                'opacity': 0,
                'overflow': 'hidden',
            });

            setTimeout(function() {
                newImage.css({
                    'transition': 'all 1.5s, opacity 0s',
                    'opacity': 1,
                });
            }, 800)


            setTimeout(function() {
                newImage.css({
                    'left': 0 + 'px',
                    'top': 0 + 'px',
                    'width': '100%',
                    'height': 415 + 'px',
                    'margin': 'auto',
                    'overflow': 'hidden',
                    'transform': 'translate(0%, 0%)',
                    '-ms-transform': 'translate(0%, 0%)',
                    '-webkit-transform': 'translate(0%, 0%)',
                })

                _this.addClass('active');

                $('.project-main,.header').css({
                    'opacity': 0,
                    'transition': '.2s'
                })
            }, 1500);


            setTimeout(function() {
                newImage.css({
                        'left': 0,
                        'top': 0 + 'px',
                    })
                    // $('.project-detail-view').load('projects-innner-template.php',{id: _this.attr('data-id')}, function() {
                $('.project-detail-view').load('projects-innner-template.html', function() {
                    $(window).scrollTop(0);
                    window.location.hash += '?id=' + _this.attr('data-id');
                    $('.header-target .img-wrapper').fadeOut();
                    $('.main-view').hide();
                    $('.project-detail-view .header__background').attr('src', headerImg);
                    // debugger
                    aspectRatioProjectHeader();
                    initProjectInnerSlider();
                    bindInView();
                    sliderBtnClick();

                    $('.slider-row img').load(function() {
                        $('.slider-row').slick({
                            infinite: true,
                            speed: 300,
                            slidesToShow: 1,
                            centerMode: true,
                            variableWidth: true,
                            arrows: false
                        });
                    })
                });

                $('.project-main,.header').css({
                    'opacity': 1,
                    'transition': '.2s 1s'
                })
            }, 3000);
        } else {
            newImagePos = {
                left: _this.offset().left + 15,
                top: (_this.offset().top - $(window).scrollTop()),
                originWidth: $(this).outerWidth(),
                originHeight: $(this).outerHeight(),
                scrollPosition: $(window).scrollTop(),
                offsetTop: $(this).offset().top
            };

            newImage.css({
                'position': 'fixed',
                'z-index': 10,
                'top': newImagePos.top + 'px',
                'left': newImagePos.left + 'px',
                'margin': 'auto',
                'transform': 'translate(0%, 0%)',
                '-ms-transform': 'translate(0%, 0%)',
                '-webkit-transform': 'translate(0%, 0%)',
                'transform-origin': '0% 50%',
                'transition': '1.5s ease',
                'overflow': 'hidden',
            });

            // debugger

            if (isFirefox || (ieDetector.ieVersion != false)) {
                setTimeout(function() {
                    $('.header-target').css({
                        'opacity': 1
                    })
                }, 600)
            }

            setTimeout(function() {
                newImage.css({
                    'left': '0',
                    'top': '0',
                    'width': '100%',
                    'height': $(window).outerHeight(),

                });
                _this.addClass('active');
            }, 1000);

            setTimeout(function() {
                newImage.removeClass('masked');
            }, 1800)

            setTimeout(function() {
                // $('.project-detail-view').load('projects-innner-template.php',{id: _this.attr('data-id')}, function() {
                $('.project-detail-view').load('projects-innner-template.html', function() {
                    $(window).scrollTop(0);
                    window.location.hash += '?id=' + _this.attr('data-id');
                    $('.header-target .img-wrapper').fadeOut();
                    $('.main-view').hide();
                    $('.project-detail-view .header__background').attr('src', headerImg);
                    aspectRatioProjectHeader();
                    initProjectInnerSlider();
                    bindInView();
                    mousemove();
                    sliderBtnClick();

                    $('.slider-row img').load(function() {
                        $('.slider-row').slick({
                            infinite: true,
                            speed: 300,
                            slidesToShow: 1,
                            centerMode: true,
                            variableWidth: true,
                            arrows: false
                        });
                    })

                    if (($('.img-wrapper').width() / $('.img-wrapper').height()) < ($('.img-wrapper img').outerWidth() / $('.img-wrapper img').outerHeight())) {
                        $('.img-wrapper img').removeClass('bgwidth').addClass('bgheight');
                    } else {
                        $('.img-wrapper img').removeClass('bgheight').addClass('bgwidth');
                    }
                });

                if (mobile) {
                    $('.main-view').css({
                        'opacity': 1,
                        'transition': '.2s 1s'
                    })
                }
            }, 3200);

        }
    });

    function getImagesSize(resize) {
        var delay = 0;
        // project-main__item
        $('.project-main__item img').load();
        // $('.project-main__item img').load(function() {
        // console.log('img loaded');

        $('.project-main__item').each(function(index, value) {

            $(this).find('img').css('position', 'static');

            $(this).find('.img-wrapper').css({
                'width': 'auto',
                'height': 'auto'
            });

            var tempWidth = $(this).find('img').width();
            var tempHeight = $(this).find('img').height();

            $(this).find('.img-wrapper').css({
                'width': tempWidth,
                'height': tempHeight
            });

            $(this).find('img').css('position', 'absolute');

            delay += 0.2;
            $(this).css({
                'transition-delay': delay + 's',
            })

            if (index == 1 && ($(window).outerWidth() > 771)) {
                $(this).css({
                    'transition-delay': '0.1s',
                })
            }

        });

        $('.grid').masonry({
            itemSelector: '.grid-item',
            columnWidth: '.grid-sizer',
            stamp: '.stamp'
        });

        // })



    }

    function mousemove() {

        // console.log('mouse mouve header listener')

        $('.header-inner').mousemove(function(event) {
            var pageX = event.pageX;
            var pageY = event.pageY;

            var moveX = 0.01 * (100 - (pageX * 100 / ($('.header-inner').outerWidth() / 2)));
            var moveY = 0.01 * (100 - (pageY * 100 / ($('.header-inner').outerHeight() / 2)));

            var freeSpaceHeight = ($(window).outerHeight() - $('.header__background').outerHeight()) / 2;
            var freeSpaceWidth = ($(window).outerWidth() - $('.header__background').outerWidth()) / 2;

            if ($('.header__background').hasClass('bgwidth')) {
                $('.header-target .img-moving,.header-inner .img-moving').css({
                    'transform': 'translate(0%, ' + moveY * freeSpaceHeight + 'px)',
                    '-ms-transform': 'translate(0%, ' + moveY * freeSpaceHeight + 'px)',
                    '-webkit-transform': 'translate(0%, ' + moveY * freeSpaceHeight + 'px)',
                })
            } else if ($('.header__background').hasClass('bgheight')) {
                $('.header-target .img-moving,.header-inner .img-moving').css({
                    'transform': 'translate(' + moveX * freeSpaceWidth + 'px, 0%)',
                    '-ms-transform': 'translate(' + moveX * freeSpaceHeight + 'px, 0%)',
                    '-webkit-transform': 'translate(' + moveX * freeSpaceHeight + 'px, 0%)',

                    // 'margin-left': moveX * freeSpaceWidth + 'px',
                })
            }

        });
    }


    $(document).on('click', '.js-next-project', function(e) {
        e.preventDefault();
        var _this = $(this);
        $('.project-detail-view').css({
            'opacity': 0,
            'transition': '0.5s'
        })
        var nextImagePos = null;
        nextImagePos = {
            left: $('.next-project .img-wrapper').offset().left,
            top: ($('.next-project .img-wrapper').offset().top - $(window).scrollTop()),
            originWidth: $('.next-project .img-wrapper').outerWidth(),
            originHeight: $('.next-project .img-wrapper').outerHeight(),
            offsetTop: $('.next-project img').offset().top
        };
        $('.header-target').html('');
        _this.closest('.next-project').find('.img-wrapper').clone().appendTo('.header-target');
        var newImage = $('.header-target .img-wrapper');
        var headerImg = newImage.find('img').attr('src');
        newImage.css({
            'position': 'fixed',
            'left': nextImagePos.left + 'px',
            'top': nextImagePos.top + 'px',
            'width': nextImagePos.originWidth + 'px',
            'height': nextImagePos.originHeight + 'px',
            'z-index': 50,
            'transition': '1s',
            'overflow': 'hidden'
        })
        setTimeout(function() {
            newImage.css({
                    'left': '0px',
                    'top': '0px',
                    'width': '100%',
                })
                // 'height': nextImagePos.originHeight + 'px',
            if (($(window).outerWidth() < 991) && ($(window).outerWidth() >= 771)) {
                newImage.css({
                    'height': '75vh',
                })
            } else if ($(window).outerWidth() < 771) {
                newImage.css({
                    'height': 415 + 'px'
                })
            } else {
                newImage.css({
                    'height': $(window).outerHeight(),
                })
            }
        }, 100)

        setTimeout(function() {
            $('.slider-row').slick('unslick');
            var headerImg = newImage.find('img').attr('src');
            var theWindow = $(window),
                aspectRatio = newImage.find('img').outerWidth() / newImage.find('img').outerHeight();
            if ((theWindow.width() / theWindow.height()) < aspectRatio) {
                newImage.find('img').addClass('bgheight');
                $('.project-detail-view .header__wrap img').addClass('bgheight');
            } else {
                newImage.find('img').addClass('bgwidth');
                $('.project-detail-view .header__wrap img').addClass('bgwidth');
            }
        }, 1500);

        setTimeout(function() {
            window.location.hash = '#projects?id=' + _this.attr('data-id');
            aspectRatioHeaderBg();

            setTimeout(function() {
                $(window).scrollTop(0);

                // $('.project-detail-view').load('projects-innner-template.php',{id: _this.attr('data-id')}, function() {
                $('.project-detail-view').load('projects-innner-template.html', function() {
                    $(window).scrollTop(0);
                    $('.project-detail-view .header__background').attr('src', headerImg);
                    // console.log('next project show');

                    var targetProjectId = _this.attr('data-id');
                    var targetActive = $('.project-main__wrapper .project-main__item.active');
                    targetActive.removeClass('active').show();
                    $('.project-main__item[data-id=' + targetProjectId + ']').addClass('active');
                    mousemove();
                    aspectRatioProjectHeader();
                    initProjectInnerSlider();
                    bindInView();
                    sliderBtnClick();

                    $('.slider-row img').load(function() {
                        $('.slider-row').slick({
                            infinite: true,
                            speed: 300,
                            slidesToShow: 1,
                            centerMode: true,
                            variableWidth: true,
                            arrows: false
                        });
                    })

                    if (($('.img-wrapper').width() / $('.img-wrapper').height()) < ($('.img-wrapper img').outerWidth() / $('.img-wrapper img').outerHeight())) {
                        $('.img-wrapper img').removeClass('bgwidth').addClass('bgheight');
                    } else {
                        $('.img-wrapper img').removeClass('bgheight').addClass('bgwidth');
                    }




                    $('.project-detail-view').css({
                        'opacity': 1,
                        //    'transition': 'none',
                    })


                    setTimeout(function() {
                        $('.header-target .img-wrapper').fadeOut();
                    }, 800)



                });
            }, 400);
        }, 2000)
    });

    var positionForBack = false;

    $(document).on('click', '.js-project-back', function(e) {
        e.preventDefault();
        projectBackFn();
    });

    function projectBackFn() {
        $('.logo--small').fadeIn();
        $('.slider-row').slick('unslick');
        if (mobile) {

        }

        openProj = false;

        $('.header__left-btn,.header__right-btn').css({
            'z-index': 6,
        })
        var targetImg = $('.header-target .img-wrapper');
        targetImg.show();

        $('.img-moving').css({
            'transform': 'translate(0%, 0%)',
            '-ms-transform': 'translate(0%, 0%)',
            '-webkit-transform': 'translate(0%, 0%)',
            'transition': 'transform 1s',
        })

        // console.log(newImagePos.scrollPosition);

        $('.main-view').show();
        $('.project-detail-view').html('');

        // console.log(positionForBack);

        newImagePos.left = $('.project-main__wrapper .project-main__item.active').offset().left + 15;


        newImagePos.top = $('.project-main__wrapper .project-main__item.active').offset().top - $(window).scrollTop() - $('.project-main__wrapper .project-main__item.active').outerHeight();

        if (!(positionForBack)) {
            $(window).scrollTop(newImagePos.scrollPosition);
        } else {
            $(window).scrollTop($('.project-main__item.active').offset().top - ($(window).outerHeight() / 2))
        }

        // debugger

        // debugger

        if (ios) {
            setTimeout(function() {
                getImagesSize();
            }, 500);
        }

        if (mobile) {
            targetImg.css({
                'opacity': 0,
                'transition': '.4s ease'
            })
            $('.grid').masonry('destroy')
            $('.grid').masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                stamp: '.stamp'
            });

            $('.project-main__item.active').fadeIn();
            $('.project-main__item.active').removeClass('active');

            setTimeout(function() {

                $('.header__menu').fadeIn();
            }, 400)

            setTimeout(function() {
                //
                // targetImg.fadeOut();
                window.location.hash = '#projects';
                $('.project-main__item.active').removeClass('active');
                $('.header-target').html('').removeClass('horizontal vertical');
            }, 1500)


        } else {
            setFooterHeight();

            targetImg.css({

                // 'transition': '1s ease',
                // 'transform': 'translate(0%, 0%)',
                // '-ms-transform': 'translate(0%, 0%)',
                // '-webkit-transform': 'translate(0%, 0%)',
            })

            targetImg.css({

                // 'left': '50%',
                // 'top': '50%',
                // 'width': '100%',
                // 'height': $(window).outerHeight(),
                'position': 'fixed',
                'z-index': 10,
                'top': 0,
                'left': 0,
                // 'margin': 'auto',

                'transform': 'translate(-50% -50%)',
                '-ms-transform': 'translate(-50% -50%)',
                '-webkit-transform': 'translate(-50% -50%)',
                'transition': '1s ease',
                'opacity': 1,
            });

            // debugger;

            setTimeout(function() {
                $('.project-main__item.active').fadeIn();

                targetImg.css({
                    'width': $('.project-main__wrapper .project-main__item.active').outerWidth() - 30,
                    'height': $('.project-main__wrapper .project-main__item.active').outerHeight(),
                    'left': $('.project-main__item.active').offset().left + 15,
                    'top': $('.project-main__item.active').offset().top - $(window).scrollTop(),
                    'transform': 'translate(0 0)',
                    '-ms-transform': 'translate(0 0)',
                    '-webkit-transform': 'translate(0 0)',
                    // 'transition': '1s ease',
                    'margin': 'auto',
                    'overflow': 'hidden',
                });

                $('.header__menu').fadeIn();
                // console.log(newImagePos.left);
            }, 500);

            setTimeout(function() {
                //
                targetImg.fadeOut();
                window.location.hash = '#projects';
                $('.project-main__item.active').removeClass('active');
                $('.header-target').html('').removeClass('horizontal vertical');
            }, 1500)

        }

        // firstVisit = true;
    }

    var newImagePos = {};

    $('.stamp').each(function() {
        $(this).next().css({
            'height': $(this).outerHeight()
        })
    });

    function scrollFired() {
        // console.log('scrollFired');

        var firstVisit = true;
        var fistLookTeam = false;

        if (ieDetector != false) {
            $('body').scroll(scrollFn);
        }

        window.addEventListener('scroll', function() {
            scrollFn();
        });

        function scrollFn() {
            var targetBlack = $(document).find('.target-black');
            var logo = $(document).find('.logo');
            var rightBtn = $(document).find('.header__right-btn');
            var leftBtn = $(document).find('.header__left-btn');


            if ($(document).find('.info-wrapper').length > 0) {
                var mainBlockPos = $(document).find('.info-wrapper').offset().top;
            }
            var scrollTop = $(window).scrollTop();
            if (scrollTop > (mainBlockPos - logo.height() / 2)) {
                targetBlack.addClass('black');
                // $('.logo--small ').hide();
            } else {
                targetBlack.removeClass('black');

                // $('.logo--small ').show();
                targetBlack.addClass('fixed');
            }
            if (scrollTop > mainBlockPos) {
                targetBlack.removeClass('fixed');
            } else {
                targetBlack.addClass('fixed');
            }

            if ($('.team-img-hover').length > 0) {
                if (scrollTop > $('.bureau__team').offset().top && scrollTop < ($('.bureau__team').offset().top + $('.bureau__team').outerHeight() - $('.team-img-hover').outerHeight() - 50)) {
                    // console.log('yeas');
                    var moving = scrollTop - $('.bureau__team').offset().top
                    $('.team-img-hover').css({
                        // 'top' :'0px',
                        'transform': 'translateY(' + moving + 'px)',
                        // 'bottom' : 'auto',
                        'transition': 'transform 1s cubic-bezier(0.23, 1, 0.32, 1)'
                    })
                } else if (scrollTop > ($('.bureau__team').offset().top + $('.bureau__team').outerHeight() - $('.team-img-hover').outerHeight())) {
                    // console.log('bottom');
                    // $('.team-img-hover').removeClass('top');
                }
            }

            if (!(mobile)) {

                if (scrollTop > $('.main-view .info-wrapper').offset().top) {
                    $('.footer').css({
                        'z-index': 7
                    })
                    $('.info-wrapper').css({
                        'z-index': 8
                    })
                } else {
                    $('.footer').css({
                        'z-index': -1
                    })
                    $('.info-wrapper').css({
                        'z-index': 7
                    })
                }
            }


            if (leftBtn.length > 0 && !(leftBtn.hasClass('to-center')) && !(mobile)) {
                if (scrollTop > (leftBtn.offset().top - scrollTop)) {
                    leftBtn.addClass('black');

                } else {
                    leftBtn.removeClass('black');
                }


                if (scrollTop > (leftBtn.offset().top - scrollTop)) {
                    leftBtn.addClass('black').css({
                        'z-index': 9,
                    });
                } else {
                    leftBtn.removeClass('black').css({
                        'z-index': 6,
                    });
                }

                if (openProj) {
                    leftBtn.removeClass('black').css({
                        'z-index': 0,
                    });
                }



            }

            if (rightBtn.length > 0 && !(rightBtn.hasClass('to-center')) && !(mobile) && pageState == "bureau") {

                if (scrollTop > (rightBtn.offset().top - scrollTop)) {
                    rightBtn.addClass('black').css({
                        'z-index': 9,
                    });
                } else {
                    rightBtn.removeClass('black').css({
                        'z-index': 6,
                    });
                }

                if (
                    ((scrollTop + (rightBtn.offset().top - scrollTop)) > $(document).find('.bureau__slider').offset().top) &&
                    ((scrollTop + (rightBtn.offset().top - scrollTop)) < ($(document).find('.bureau__slider').offset().top + $(document).find('.bureau__slider').outerHeight())) ||
                    ((scrollTop + (rightBtn.offset().top - scrollTop)) > $(document).find('.honors').offset().top) &&
                    ((scrollTop + (rightBtn.offset().top - scrollTop)) < ($(document).find('.honors').offset().top + $(document).find('.honors').outerHeight()))
                ) {
                    rightBtn.addClass('white');
                } else {
                    rightBtn.removeClass('white');
                }
            }

            if (($(window).outerWidth() > 991) && ($('.bureau__team-list').length > 0) && (!(ios))) {

                if (scrollTop > $('.bureau__team-list').offset().top && (scrollTop < ($('.bureau__team').offset().top + $('.bureau__team').outerHeight() - $(window).outerHeight()))) {
                    $('.bureau__team-title').css({
                        'position': 'fixed',
                        'left': $('.bureau__team > .container').offset().left + 15,
                        'top': '0',
                    });

                    var teamListMoving = scrollTop - $('.bureau__team-list').offset().top;

                    if (teamListMoving <= 0 && fistLookTeam) {
                        teamListMoving = 0;
                        return
                    } else if (fistLookTeam) {
                        $('.new-list').eq(0).css({
                            'transform': 'translateY(-' + teamListMoving * 0.1 + 'px)',
                            '-ms-transform': 'translateY(-' + teamListMoving * 0.1 + 'px)',
                            '-webkit-transform': 'translateY(-' + teamListMoving * 0.1 + 'px)',
                            'transition-delay': '0s',
                        })
                        $('.new-list').eq(1).css({
                            'transform': 'translateY(-' + teamListMoving * 0.7 + 'px)',
                            '-ms-transform': 'translateY(-' + teamListMoving * 0.7 + 'px)',
                            '-webkit-transform': 'translateY(-' + teamListMoving * 0.7 + 'px)',
                            'transition-delay': '0s',
                        })
                        $('.new-list').eq(2).css({
                            'transform': 'translateY(-' + teamListMoving * 0.5 + 'px)',
                            '-ms-transform': 'translateY(-' + teamListMoving * 0.5 + 'px)',
                            '-webkit-transform': 'translateY(-' + teamListMoving * 0.5 + 'px)',
                            'transition-delay': '0s',
                        })
                    }
                } else {
                    $('.bureau__team-title').css({
                        'position': 'absolute',
                        'left': 'inherit',
                    });
                }

                if (scrollTop < $('.bureau__team-list').offset().top && fistLookTeam) {
                    $('.new-list').eq(0).css({
                        'transform': 'translateY(0)',
                        '-ms-transform': 'translateY(0)',
                        '-webkit-transform': 'translateY(0)',
                    })
                    $('.new-list').eq(1).css({
                        'transform': 'translateY(0)',
                        '-ms-transform': 'translateY(0)',
                        '-webkit-transform': 'translateY(0)',
                    })
                    $('.new-list').eq(2).css({
                        'transform': 'translateY(0)',
                        '-ms-transform': 'translateY(0)',
                        '-webkit-transform': 'translateY(0)',
                    })
                }
                if (scrollTop > ($('.bureau__team').offset().top - $('.bureau__team').outerHeight() / 2) && (scrollTop < ($('.bureau__team').offset().top + $('.bureau__team').outerHeight()))) {
                    $('.bureau__team').addClass('animation');
                    fistLookTeam = true
                }
            }

            if (($(window).outerWidth() > 991) && ($('.bureau__team-list').length > 0) && (ios)) {
                if (scrollTop > ($('.bureau__team').offset().top - $('.bureau__team').outerHeight() / 2) && (scrollTop < ($('.bureau__team').offset().top + $('.bureau__team').outerHeight()))) {
                    $('.bureau__team').addClass('animation');
                    fistLookTeam = true
                }

            }


            if ($('.bureau__slider').length > 0) {
                if (scrollTop > ($('.bureau__slider').offset().top - $('.bureau__slider').outerHeight() / 2) && (scrollTop < ($('.bureau__slider').offset().top + $('.bureau__slider').outerHeight()))) {
                    $('.bureau__slider-info').addClass('animation');
                }
            }



            // console.log($('.header__right-btn').length > 0);



            if (scrollTop > ($('.header__wrap').outerHeight() / 2)) {
                $('.project-main__wrapper:first .grid , .project-main').addClass('animation');
                // bindInView()

                setTimeout(function() {
                    $('.project-main__item').css({
                        'transition-delay': '0s'
                    })
                }, 500)
            }

            //проверка на скролл на странице с проектами


            if ((scrollTop > 100) && (scrollTop < $(window).outerHeight() / 2) && firstVisit) {
                firstVisit = !firstVisit;
            }

            if (scrollTop > $(window).outerHeight() / 2) {
                firstVisit = false;
            }
        }
    };

    scrollFired();

    window.addEventListener('load', function() {
        setTimeout(function() {
            if ($(window).scrollTop() > ($('.header__wrap').outerHeight() / 2)) {
                $('.grid , .project-main').addClass('animation');
            }


        }, 100)

        getImagesSize();


    });

    function getHeight() {
        if (mobile) {
            $('.js-height,.view .page-wrapper').css({
                'min-height': $(window).outerHeight(),
                // 'height' : $(window).outerHeight(),
            });

            $('.fixed-elements').css({
                'height': $(window).outerHeight(),
                'position': 'absolute',
                'top': 0,
                'width': '100%'
            })

        }

        if (ios) {
            $('.js-height,.view .page-wrapper').css({
                'min-height': $(window).outerHeight(),
            });
            $('.view').css({
                'min-height': 'auto'
            })
        }
    }

    $(window).on('load', function() {
        getHeight()
    });

    $(window).on('resize', function() {
        if (!(mobile) && !(ios)) {
            getHeight()
        }
    })

    var trnDuration = 1500;
    //****************************************************************************************************
    function showVideo(pageLocation) {

        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
        var videoArray = [];
        for (var i = 0; i < 3; i++) {
            $(function checkNum() {
                var tempRandom = getRandomInt(0, 7);
                if (videoArray.length == 0) {
                    videoArray.push(tempRandom);
                } else {
                    videoArray.forEach(function(name, key) {
                        if (name == tempRandom) {
                            checkNum();
                        } else {
                            if (key == (videoArray.length - 1)) {
                                videoArray.push(tempRandom)
                                return false
                            }
                        }
                    })
                }
            })
        }
        $('.loader video').attr('style', '');
        $('.loader video').css({
            'position': 'absolute',
            'opacity': 0
        })
        $('.Perebivka').eq(videoArray[0]).css({
            'opacity': 1,
            'z-indez': 1,
        })
        // console.log(videoArray);
        $('.Perebivka').eq(videoArray[0]).get(0).play();
        var duration1 = $('.Perebivka').eq(videoArray[0]).get(0).duration * 1000;
        var duration2 = $('.Perebivka').eq(videoArray[1]).get(0).duration * 1000;
        var duration3 = $('.Perebivka').eq(videoArray[2]).get(0).duration * 1000;
        setTimeout(function() {
            $('.Perebivka').eq(videoArray[1]).get(0).play();
            $('.Perebivka').eq(videoArray[1]).css({
                'opacity': 1,
                'z-index': 2,
                'transition': '.1s'
            })
            setTimeout(function() {
                $('.Perebivka').eq(videoArray[0]).attr('style', '');
                $('.Perebivka').eq(videoArray[2]).get(0).play();
                $('.Perebivka').eq(videoArray[2]).css({
                    'opacity': 1,
                    'z-index': 3,
                    'transition': '.1s'
                })
                setTimeout(function() {
                    $('.' + pageLocation).css({
                        'opacity': 1,
                        'z-index': 4,
                        'transition': '.1s'
                    })
                    $('.' + pageLocation).get(0).play();
                }, (duration3 - 100))
            }, (duration2 - 100))
        }, (duration1 - 100))
    }
    $('.header__left-btn').on('click', function(e) {
        e.preventDefault();
        if (pageState == 'bureau') {
            return;
        }
        if (pageState == 'index') {
            $(window).scrollTop(0)
        }

        saveOverlay.show();
        var _this = $(this);
        var targetRoute = _this.attr('href');

        $('.logo--small').addClass('per-page');


        if (mobile) {
            $('.view').css({
                'opacity': 0,
                'transition': '.3s'
            })

            setTimeout(function() {
                location.hash = targetRoute;
            }, 300);

            setTimeout(function() {
                $('.view').css({
                    'opacity': 1,
                    'transition': 'opacity .3s ease'
                })
            }, 1000);
        } else {

            _this.unbind('mouseout').unbind('mouseover');

            $('.project-main,.footer').css({
                'transform': 'translateX(100%)',
                '-ms-transform': 'translateX(100%)',
                '-webkit-transform': 'translateX(100%)',
                'transition': 'transform .5s ease'
            })

            _this.removeClass('black');


            if (ios) {
                _this.addClass('to-center').css({
                    'transition': ' all 1s ease 1s, transform .2s 1s',
                    // 'transform' : 'translate(-100%, 50%)'
                })
            } else if (ieDetector.ieVersion == false) {
                _this.addClass('to-center').css({
                    'transition': '1s ease',
                    'transition-delay': '.5s',
                });
            } else {
                _this.addClass('to-center');
            }
            $('.logo--index-page').fadeOut();
            $('.loader').fadeIn(400);

            if (!(mobile)) {
                showVideo('BURO');
            }

            setTimeout(function() {
                _this.find('.icon').css({
                    'transform': 'translateX(-10px)',
                    '-ms-transform': 'translateX(-10px)',
                    '-webkit-transform': 'translateX(-10px)',
                    'opacity': '0',
                });
            }, 800)
            setTimeout(function() {
                location.hash = targetRoute;
            }, 3000)

            if ($('.header__right-btn').hasClass('to-center')) {
                $('.header__right-btn').removeClass('to-center').css({
                    'transition': '1s ease',
                    'transition-delay': '.5s'
                });

                setTimeout(function() {
                    $('.header__right-btn').css({
                        'transition': '0.1s ease',
                    });
                    $('.header__right-btn').find('.icon').css({
                        'transform': 'translateX(0px)',
                        '-ms-transform': 'translateX(0px)',
                        '-webkit-transform': 'translateX(0px)',
                        'transition': '0.4s ease',
                        'opacity': 1
                    });
                }, 2000)
            }
        }
    });




    //****************************************************************************************************

    $('.header__right-btn').on('click', function(e) {
        e.preventDefault();
        if (pageState == 'projects') {
            return;
        }

        saveOverlay.show()



        if (pageState == 'index') {
            $(window).scrollTop(0)
        }
        var _this = $(this);
        var targetRoute = _this.attr('href');

        $('.logo--small').addClass('per-page');


        if (mobile) {
            // console.log();
            $('.view').css({
                'opacity': 0,
                'transition': '.3s'
            })

            setTimeout(function() {
                location.hash = targetRoute;
            }, 300);

            setTimeout(function() {
                $('.view').css({
                    'opacity': 1,
                    'transition': 'opacity .3s ease'
                })
            }, 1000);
        } else {

            _this.unbind('mouseout').unbind('mouseover');

            $('.bureau,.footer').css({
                'transform': 'translateX(-100%)',
                '-ms-transform': 'translateX(-100%)',
                '-webkit-transform': 'translateX(-100%)',
                'transition': 'transform .5s ease'
            })



            _this.removeClass('black');
            // _this.addClass('to-center').css({
            //     'transition': '2s ease',
            //     'transition-delay': '1s'
            // });


            if (ios) {
                _this.addClass('to-center').css({
                    'transition': ' all 1s ease 1s, transform .2s 1s',
                    // 'transform' : 'translate(-100%, 50%)'
                })
            } else if (ieDetector.ieVersion == false) {
                _this.addClass('to-center').css({
                    'transition': '1s ease',
                    'transition-delay': '.5s',
                });
            } else {
                _this.addClass('to-center');
            }


            // return;


            $('.logo--index-page').fadeOut();
            $('.loader').fadeIn(400);
            if (!(mobile)) {
                if (!(mobile)) {
                    showVideo('PROJECTS');
                }
            }

            setTimeout(function() {
                _this.find('.icon').css({
                    'transform': 'translateX(10px)',
                    '-ms-transform': 'translateX(10px)',
                    '-webkit-transform': 'translateX(10px)',
                    'opacity': '0',
                });
            }, 800)
            setTimeout(function() {
                location.hash = targetRoute;
            }, 2000)

            if ($('.header__left-btn').hasClass('to-center')) {
                $('.header__left-btn').removeClass('to-center').css({
                    'transition': '1s ease',
                    'transition-delay': '.5s'
                });

                setTimeout(function() {
                    $('.header__left-btn').css({
                        'transition': '0.1s ease',
                    });
                    $('.header__left-btn').find('.icon').css({
                        'transform': 'translateX(0px)',
                        '-ms-transform': 'translateX(0px)',
                        '-webkit-transform': 'translateX(0px)',
                        'transition': '0.4s ease',
                        'opacity': 1
                    });
                }, 2000)
            }
        }
    });


    function sliderBtnClick() {
        $(document).on('click', '.js-project-slide', function(e) {
            e.preventDefault();
            switch ($(this).attr('data-direction')) {
                case 'right':
                    $('.slider-row').slick('slickNext')
                    $('.slide-btn-left').show()
                    break;
                case 'left':
                    $('.slider-row').slick('slickPrev')
                    break;
                default:

            }
            // $('.slider-row').slick('slickNext')
        });
    }

    function navBtns() {

        var arrowBlack = false;
        var arrowWhite = false;
        var menuBlack = false;

        $('.header__right-btn--index-page').on('mouseover', function() {
            if (pageState == 'index') {
                $('.header__bg-wrapper').css({
                    'transform': 'translateX(-20px)',
                    '-ms-transform': 'translateX(-20px)',
                    '-webkit-transform': 'translateX(-20px)',
                    'transition': '1s ease'
                })
                $(this).find('.icon').css({
                    'transform': 'translateX(5px)',
                    '-ms-transform': 'translateX(5px)',
                    '-webkit-transform': 'translateX(5px)',
                    'transition': '0.4s ease'
                })
            }

            if (pageState == 'bureau' && !(mobile)) {
                if ($(this).hasClass('black')) {
                    $(this).removeClass('black');
                    arrowBlack = true;
                };

                if ($(this).hasClass('white')) {
                    $(this).removeClass('white');
                    arrowWhite = true;
                };

                if ($('.header__menu ').hasClass('black')) {
                    $('.header__menu ').removeClass('black');
                    menuBlack = true;
                }
                $('.bureau,.footer').css({
                    'transform': 'translateX(-370px)',
                    '-ms-transform': 'translateX(-370px)',
                    '-webkit-transform': 'translateX(-370px)',
                    'transition': 'transform .4s ease'
                })
            }
        });



        $('.header__right-btn--index-page').on('mouseout', function() {
            if (pageState == 'index') {
                $('.header__bg-wrapper').css({
                    'transform': 'translateX(0px)',
                    '-ms-transform': 'translateX(0px)',
                    '-webkit-transform': 'translateX(0px)',
                    'transition': '1s ease'
                })

                $(this).find('.icon').css({
                    'transform': 'translateX(0px)',
                    '-ms-transform': 'translateX(0px)',
                    '-webkit-transform': 'translateX(0px)',
                    'transition': '0.4s ease'
                })
            }

            if (pageState == 'bureau' && !(mobile)) {
                if (arrowBlack) {
                    $(this).addClass('black');
                }

                if (arrowWhite) {
                    $(this).addClass('white');
                }

                if (menuBlack) {
                    $('.header__menu ').addClass('black');
                }
                $('.bureau,.footer').css({
                    'transform': 'translateX(0px)',
                    '-ms-transform': 'translateX(0px)',
                    '-webkit-transform': 'translateX(0px)',
                    'transition': 'transform .4s ease'
                })
            }
        });


        $('.header__left-btn--index-page').on('mouseover', function() {
            if (pageState == 'index') {
                $('.header__bg-wrapper').css({
                    'transform': 'translateX(20px)',
                    '-ms-transform': 'translateX(20px)',
                    '-webkit-transform': 'translateX(20px)',
                    'transition': '1s ease'
                });

                $(this).find('.icon').css({
                    'transform': 'translateX(-5px)',
                    '-ms-transform': 'translateX(-5px)',
                    '-webkit-transform': 'translateX(-5px)',
                    'transition': '0.4s ease'
                })
            }

            if (pageState == 'projects' && !(mobile)) {
                if ($(this).hasClass('black')) {
                    $(this).removeClass('black');
                    arrowBlack = true;
                }

                $('.view .logo').removeClass('fixed');
                // $('.header__menu ').removeClass('black');
                $('.project-main,.footer').css({
                    'transform': 'translateX(370px)',
                    '-ms-transform': 'translateX(370px)',
                    '-webkit-transform': 'translateX(370px)',
                    'transition': 'transform .4s ease'
                })
            }
        });

        $('.header__left-btn--index-page').on('mouseout', function() {
            if (pageState == 'index') {
                $('.header__bg-wrapper').css({
                    'transform': 'translateX(0px)',
                    '-ms-transform': 'translateX(0px)',
                    '-webkit-transform': 'translateX(0px)',
                    'transition': '1s ease'
                })

                $(this).find('.icon').css({
                    'transform': 'translateX(0px)',
                    '-ms-transform': 'translateX(0px)',
                    '-webkit-transform': 'translateX(0px)',
                    'transition': '0.4s ease'
                })
            }

            if (pageState == 'projects' && !(mobile)) {
                if (arrowBlack) {
                    $(this).addClass('black');
                }
                // $('.logo--small').fadeIn();
                $('.project-main,.footer').css({
                    'transform': 'translateX(0px)',
                    '-ms-transform': 'translateX(0px)',
                    '-webkit-transform': 'translateX(0px)',
                    'transition': 'transform .4s ease'
                })
            }
        });
        // console.log('test');
    }
    //****************************************************************************************************
    $(document).on('click', '.logo', function(e) {
        e.preventDefault();
        if (pageState == 'index') {
            return;
        }

        $(this).fadeOut();
        indexPageFn();
    })

    function indexPageFn() {

        saveOverlay.show();
        if (mobile) {
            $('.view').css({
                'opacity': 0,
                'transition': 'opacity .3s ease'
            })

            setTimeout(function() {
                location.hash = '';
            }, 300);

            setTimeout(function() {
                $('.view').css({
                    'opacity': 1,
                    'transition': '.3s ease'
                })
            }, 1000);
        } else {
            $('.loader').fadeIn(400);
            if (!(mobile)) {
                showVideo('MAIN');
            }
            var targetBtns = $('.header__left-btn').hasClass('to-center') ? $('.header__left-btn') : $('.header__right-btn');
            // console.log(targetBtns);
            targetBtns.removeClass('to-center').css({
                'transition': '1s ease',
                'transition-delay': '0.5s'
            });
            setTimeout(function() {
                targetBtns.find('.icon').css({
                    'transform': 'translateX(0px)',
                    '-ms-transform': 'translateX(0px)',
                    '-webkit-transform': 'translateX(0px)',
                    'transition': '0.4s ease',
                    'opacity': 1
                });
                $('.logo--small').fadeOut(400);
            }, 2000)
            setTimeout(function() {

                location.hash = '';
            }, 3000)
        }
    }

    var view = $('.view');
    var firstVisit = false;

    routie({
        'bureau': function() {
            saveOverlay.hide();
            removeIndexStyle();
            firstVisit = true;
            pageState = 'bureau';
            view.css({
                'min-height': '101vh'
            })
            var targetBtns = $('.header__left-btn').hasClass('to-center') ? $('.header__right-btn') : $('.header__left-btn');
            var sideBtn = $('.header__left-btn').hasClass('to-center') ? $('.header__left-btn') : $('.header__right-btn');
            targetBtns.css('z-index', 6);
            sideBtn.css('z-index', 6);
            $('.header__left-btn,.header__right-btn').css({
                'opacity': 1
            })
            // view.html('').load('bureau-template.php', function() {
            view.html('').load('bureau-template.html', function() {
                $(window).scrollTop(0);
                $('.logo.fixed').removeClass('fixed');
                $('.header__left-btn').addClass('to-center').css({
                    'transition': 'all 0s',
                });
                $('.header__left-btn').find('.icon').css({
                    'transform': 'translateX(10px)',
                    'opacity': '0',
                });

                $('.header__right-btn').removeClass('to-center');
                $('.header__right-btn').find('.icon').css({
                    'transform': 'translateX(0px)',
                    '-ms-transform': 'translateX(0px)',
                    '-webkit-transform': 'translateX(0px)',
                    'opacity': '1',
                });
                aspectRatioHeaderBg()
                setTimeout(function() {
                    if (ieDetector.ieVersion != false) {
                        $('.header__left-btn,.header__right-btn').attr('style', '');
                    }
                    $('.loader').fadeOut(400);
                    if (!(mobile)) {
                        $('.logo--small').fadeIn(400)
                    } else {
                        // $('.logo--small').hide()
                    }

                    if (mobile) {
                        $('.logo--small').css('display', 'block');
                    }
                }, 400)
                view.css({
                        'min-height': 'inherit'
                    })
                    //lets fire all scripts
                    // scrollFired();

                bureauFire();
                bindInView();
                navBtns();
                getHeight();
                setTimeout(function() {
                    function updateHeight() {
                        var oldHeight = 0;
                        var targetHeight = $(document).find('.honors__list > ul').outerHeight();
                        $('.honors__list ul li').each(function(key) {
                            oldHeight += $(this).outerHeight();
                            if (key === parseInt($('.honors__list').attr('data-show-more'))) {
                                return
                            }
                        })
                        $('.honors__list').css({
                            'height': oldHeight
                        });
                        $('.js-show-all-honors').on('click', function(e) {
                            e.preventDefault();
                            var _this = $(this);
                            // console.log('click to show honors');

                            if (_this.hasClass('active')) {
                                _this.removeClass('active');
                                $(document).find('.honors__list').css({
                                    'height': oldHeight
                                })
                            } else {
                                _this.addClass('active');
                                $(document).find('.honors__list').css({
                                    'height': targetHeight + 'px'
                                })
                            }
                        });
                    };
                    updateHeight();
                    setFooterHeight();
                    // $(window).on('load resize', function() {
                    // updateHeight();
                    // });
                }, 100)
            });
        },

        'projects/?:name': function(name) {
            saveOverlay.hide();
            // console.log('inner project route');
            removeIndexStyle();
            firstVisit = true;
            if (name.indexOf('id') > 0) {
                if (pageState == 'projects') {
                    return;
                }

                view.css({
                    'opacity': 0
                });

                var targetDataId = name.replace('?id=', '');

                console.log('projects inner');
                pageState = 'projects inner';
                view.css({
                    'min-height': '101vh'
                })

                var targetBtns = $('.header__left-btn').hasClass('to-center') ? $('.header__left-btn') : $('.header__right-btn');

                $('.header__left-btn,.header__right-btn').css({
                    'opacity': 1
                })

                // targetBtns.css('z-index', 6);

                // view.html('').load('projects-template.php',{id: targetDataId}, function() {
                view.html('').load('projects-template.html', function() {

                    openProj = true;
                    $(window).scrollTop(0);

                    $('.logo.fixed').removeClass('fixed');

                    // $('.header__left-btn,.header__right-btn').css({
                    //     'z-index': '0',
                    // })

                    $('.header__right-btn').addClass('to-center').css({
                        'transition': 'all 0'
                    });
                    $('.header__right-btn').find('.icon').css({
                        'transform': 'translateX(10px)',
                        '-ms-transform': 'translateX(10px)',
                        '-webkit-transform': 'translateX(10px)',
                        'opacity': '0',
                    });

                    $('.header__left-btn').removeClass('to-center');
                    $('.header__left-btn').find('.icon').css({
                        'transform': 'translateX(0px)',
                        '-ms-transform': 'translateX(0px)',
                        '-webkit-transform': 'translateX(0px)',
                        'opacity': '1',
                    });

                    aspectRatioHeaderBg()
                        // $('.grid').masonry('destroy')


                    setTimeout(function() {
                        $('.loader').fadeOut(400);
                        if (!(mobile)) {
                            $('.logo--small').fadeIn(400)
                        } else {
                            $('.logo--small').hide()
                        }

                        $('.logo--small.per-page').fadeOut(0);
                        bindInView();
                        getImagesSize();

                        if (ios) {
                            setTimeout(function() {
                                getImagesSize();
                            }, 500);
                        }
                        navBtns();
                        getHeight();

                        $('.grid-item[data-id=' + targetDataId + ']').find('.img-wrapper').clone().appendTo('.header-target');
                        $('.grid-item[data-id=' + targetDataId + ']').addClass('active');

                        // $('.project-detail-view').load('projects-innner-template.php',{id: targetDataId}, function() {
                        $('.project-detail-view').load('projects-innner-template.html', function() {

                            view.css({
                                'opacity': 1,
                                'transition': 'opacity .4s ease'
                            });

                            $('.grid,.project-main').addClass('animation');

                            $('.logo--small').fadeOut();


                            $('.header__left-btn,.header__right-btn').css({
                                'z-index': '-1',
                            })
                            $('.header__menu').fadeOut();
                            $(window).scrollTop(0);
                            $('.header-target .img-wrapper').fadeOut();

                            $('.header-target .img-wrapper').css({
                                'width': '100%',
                                'height': $(window).outerHeight(),
                                'position': 'fixed',
                                'z-index': 10,
                                'top': 0,
                                'left': 0,
                                'margin': 'auto',
                                'transform': 'translate(0%, 0%)',
                                '-ms-transform': 'translate(0%, 0%)',
                                '-webkit-transform': 'translate(0%, 0%)',
                            })




                            positionForBack = true;

                            $('.main-view').hide();
                            aspectRatioProjectHeader();
                            initProjectInnerSlider();
                            bindInView();
                            mousemove();
                            navBtns();
                            sliderBtnClick();

                            $('.slider-row img').load(function() {
                                $('.slider-row').slick({
                                    infinite: true,
                                    speed: 300,
                                    slidesToShow: 1,
                                    centerMode: true,
                                    variableWidth: true,
                                    arrows: false
                                });
                            })
                            setTimeout(function() {
                                $('.logo--small').css({
                                    'opacity': 0
                                })




                                // $('.img-wrapper img').load(function() {
                                if (($('.img-wrapper').width() / $('.img-wrapper').height()) < ($('.img-wrapper img').outerWidth() / $('.img-wrapper img').outerHeight())) {
                                    $('.img-wrapper img').removeClass('bgwidth').addClass('bgheight');
                                } else {
                                    $('.img-wrapper img').removeClass('bgheight').addClass('bgwidth');
                                }
                                // })
                            }, 400)
                        });
                    }, 400)
                    view.css({
                        'min-height': 'inherit'
                    });
                });
            }
        },
        'projects': function() {
            saveOverlay.hide();
            if (pageState == 'projects') {
                return;
            }
            removeIndexStyle();
            firstVisit = true;
            console.log('projects page');
            pageState = 'projects';
            view.css({
                'min-height': '101vh'
            })

            var targetBtns = $('.header__left-btn').hasClass('to-center') ? $('.header__left-btn') : $('.header__right-btn');
            var sideBtn = $('.header__left-btn').hasClass('to-center') ? $('.header__right-btn') : $('.header__left-btn');
            targetBtns.css('z-index', 6);
            sideBtn.css('z-index', 8);
            $('.header__left-btn,.header__right-btn').css({
                'opacity': 1
            })
            var _this = $(this);
            // view.html('').load('projects-template.php',{id: _this.attr('data-id')}, function() {
            view.html('').load('projects-template.html', function() {
                $(window).scrollTop(0);
                $('.header__right-btn').addClass('to-center').css({
                    'transition': 'all 0'
                });

                $('.header__right-btn.to-center').css({
                    'z-index': '7'
                })
                $('.header__right-btn').find('.icon').css({
                    'transition': '1s',
                    'transform': 'translateX(10px)',
                    '-ms-transform': 'translateX(10px)',
                    '-webkit-transform': 'translateX(10px)',

                    'opacity': '0',
                });

                $('.header__left-btn').removeClass('to-center');
                $('.header__left-btn').find('.icon').css({
                    'transform': 'translateX(0px)',
                    '-ms-transform': 'translateX(0px)',
                    '-webkit-transform': 'translateX(0px)',
                    'opacity': '1',
                });

                aspectRatioHeaderBg()

                $('.logo.fixed').removeClass('fixed');

                setTimeout(function() {
                    if (ieDetector.ieVersion != false) {
                        $('.header__left-btn,.header__right-btn').attr('style', '');
                    }
                    $('.loader').fadeOut(400);
                    if (!(mobile)) {
                        $('.logo--small').fadeIn(400)
                    } else {
                        $('.logo--small').show()
                    }
                    bindInView();
                    getImagesSize();
                    navBtns();
                    getHeight();
                    setFooterHeight();
                }, 800)
                view.css({
                    'min-height': 'inherit'
                })
            });
        },

        'contacts': function() {
            saveOverlay.hide();
            console.log('contacts page');
            firstVisit = true;
            pageState = 'contacts';
            view.css({
                    'min-height': '101vh'
                })
                // view.html('').load('contacts-template.php', function() {
                view.html('').load('contacts-template.html', function() {
                $(window).scrollTop(0);

                $('.header__right-btn,.header__left-btn').css({
                    'opacity': '0'
                });
                aspectRatioHeaderBg()
                setTimeout(function() {
                    $('.loader').fadeOut(400);
                    if (!(mobile)) {
                        $('.logo--small').fadeIn(400)
                    } else {
                        $('.logo--small').show()
                    }
                    bindInView();
                    getImagesSize();
                    navBtns();
                }, 400);
                setTimeout(function() {
                    initMap();
                }, 1000);
                view.css({
                    'min-height': 'inherit'
                })
            });
        },
        '': function() {
            console.log('home');
            saveOverlay.hide();
            pageState = 'index';
            view.css({
                'min-height': '101vh'
            })
            var targetBtns = $('.header__left-btn').hasClass('to-center') ? $('.header__left-btn') : $('.header__right-btn');
            if (!(firstVisit) && !(mobile)) {
                // console.log(firstVisit);
                $('.header__left-btn,.header__right-btn,body').css({
                    'opacity': 0
                });
                $(window).load(function() {
                    $('.header__left-btn').css({
                        'transform': 'translateX(-50px) translateY(-50%)',
                        '-ms-transform': 'translateX(-50px) translateY(-50%)',
                        '-webkit-transform': 'translateX(-50px) translateY(-50%)',
                        'opacity': '0'
                    });

                    $('.header__right-btn').css({
                        'transform': 'translateX(50px) translateY(-50%)',
                        '-ms-transform': 'translateX(50px) translateY(-50%)',
                        '-webkit-transform': 'translateX(50px) translateY(-50%)',
                    })

                    $('body').css({
                        'opacity':1,
                        'transition' : '.2s ease'
                    })

                    setTimeout(function() {
                        $('.header__left-btn,.header__right-btn').css({
                            'transform': 'translateX(0px) translateY(-50%)',
                            '-ms-transform': 'translateX(0px) translateY(-50%)',
                            '-webkit-transform': 'translateX(0px) translateY(-50%)',
                            'transition': '.5s ease',
                            'opacity': 1
                        })
                    }, 800);
                    setTimeout(function() {
                        $('.header__left-btn,.header__right-btn').attr('style', '');
                    }, 1500)

                    firstVisit = true;
                })
            } else {
                $('.header__left-btn,.header__right-btn').css({
                    'opacity': 1
                });
            }

            if (mobile) {
                $('.header__left-btn,.header__right-btn').css({
                    'opacity': 0
                });
            }

            targetBtns.removeClass('to-center').css({
                'transition': '0s ease',
                'transition-delay': '0s'
            });
            targetBtns.find('.icon').css({
                'transform': 'translateX(0px)',
                '-ms-transform': 'translateX(0px)',
                '-webkit-transform': 'translateX(0px)',
                'transition': '0s ease 0s',
                'opacity': 1
            });
            $('.logo--small').hide();
            // view.html('').load('index-template.php', function() {
            view.html('').load('index-template.html', function() {
                $(window).scrollTop(0);
                getHeight();
                $('.header__menu').show();
                setTimeout(function() {
                    $('.loader').fadeOut(400);
                    if (!(mobile)) {
                        $('.view,.fixed-elements').addClass('index');
                        setFooterHeight();
                    }
                }, 400)

                $('.header__background--index').load(function() {
                    function indexBg() {
                        var sliderImg = $('.header__background--index'),
                            theWindow = $(window),
                            aspectRatio = sliderImg.width() / sliderImg.height();
                        if (($('.header').width() / $('.header').height()) < aspectRatio) {
                            sliderImg.removeClass('bgwidth').addClass('bgheight');
                        } else {
                            sliderImg.removeClass('bgheight').addClass('bgwidth');
                        }
                    }
                    indexBg();
                    navBtns();
                    window.addEventListener('resize', indexBg);
                })
                $('.header__left-btn,.header__right-btn').css({
                    'z-index': '8',
                })
            });
        }
    });

    function removeIndexStyle() {
        if (!(mobile)) {
            $('.view,.fixed-elements').removeClass('index');
            $('.footer-placeholder').css({
                'height': $('.footer').outerHeight() + 'px',
            })
        }
    }

    function setFooterHeight() {
        $('.footer-placeholder').css({
            'height': $('.footer').outerHeight() + 'px',
        })
    }

    function menuImg() {
        var sliderImg = $('.overlay__img'),
            theWindow = $(window),
            aspectRatio = sliderImg.width() / sliderImg.height();
        if (($('.bg-wrapper').width() / $('.bg-wrapper').height()) < aspectRatio) {
            sliderImg.removeClass('bgwidth').addClass('bgheight');
        } else {
            sliderImg.removeClass('bgheight').addClass('bgwidth');
        }
    }

    // menuImg()
    window.addEventListener('resize', menuImg);
    var returnBlack = false;

    $(document).on('click', '.js-show-menu', function(e) {
        e.preventDefault();
        var _this = $(this);
        if (mobile) {
            if (_this.hasClass('active')) {
                _this.removeClass('active');
                $('.overlay').removeClass('active');
                // $('.view .logo').show();
                $('.view').css({
                    'transform': 'translateY(0px)',
                    '-ms-transform': 'translateY(0px)',
                    '-webkit-transform': 'translateY(0px)',
                    'transition': '.3s ease'
                });
                $('.fixed-elements').css({
                    'top': 0,
                    'transition': 'top .3s ease'
                })
                _this.css({
                    'transform': 'translateY(0px)',
                    '-ms-transform': 'translateY(0px)',
                    '-webkit-transform': 'translateY(0px)',
                    'transition': '.3s ease'
                });
                if (pageState != 'index') {

                }
            } else {
                _this.addClass('active');
                $('.overlay').addClass('active')
                    // $('.view .logo').hide();
                $('.view').css({
                    'transform': 'translateY(' + $('.overlay').outerHeight() + 'px)',
                    '-ms-transform': 'translateY(' + $('.overlay').outerHeight() + 'px)',
                    '-webkit-transform': 'translateY(' + $('.overlay').outerHeight() + 'px)',
                    'transition': '.3s ease'
                });
                $('.fixed-elements').css({
                    'top': $('.overlay').outerHeight(),
                    'transition': 'top .3s ease',
                })

                if (pageState == 'contacts') {
                    $('.view .logo').fadeOut();
                }
                _this.css({
                    'transform': 'translateY(' + $('.overlay').outerHeight() + 'px)',
                    '-ms-transform': 'translateY(' + $('.overlay').outerHeight() + 'px)',
                    '-webkit-transform': 'translateY(' + $('.overlay').outerHeight() + 'px)',
                    'transition': '.3s ease'
                });
            }
        } else {
            if (_this.hasClass('active')) {
                if (returnBlack) {
                    // console.log('return black menu');
                    _this.addClass('black');
                    returnBlack = false;
                }
                _this.removeClass('active');
                $('.overlay').removeClass('active')
            } else {
                if (_this.hasClass('black')) {
                    _this.removeClass('black');
                    returnBlack = true;
                }

                _this.addClass('active');
                $('.overlay').addClass('active')
            }
            menuImg();
        }
    })
    var menuBg = $('.bg-wrapper');

    function menuMouseEvents() {
        if (mobile) {
            return
        }
        $('.js-menu-bg').on('mouseover', function() {
            var directionImg = $(this).attr('data-img');
            $('.' + directionImg).css({
                'opacity': 1,
                'transition': 'opacity .2s'
            })
        })

        $('.js-menu-bg').on('mouseout', function() {
            var directionImg = $(this).attr('data-img');

            $('.' + directionImg).css({
                'opacity': 0,
                'transition': 'opacity .2s'
            })
        })
    }

    menuMouseEvents();
    $('.js-menu-bg').on('click', function(e) {
        e.preventDefault();
        // console.log('menu item click');
        var _this = $(this);
        var directionImg = $(this).attr('data-img');
        var targetRoute = _this.attr('href');
        if (targetRoute.replace('#', '') == pageState) {
            //проверка , если мы на целевоц странице - return
            // console.log('state');
            return;
        }
        $(window).scrollTop(0)

        // $('.fixed-elements.to-center').css({
            // 'transition': '0s'
        // })
        // $('.fixed-elements.to-center').removeClass('to-center');


        if (mobile) {
            $('.overlay').removeClass('active');
            $('.js-show-menu,.view').css({
                'transform': 'translateY(0)',
                '-ms-transform': 'translateY(0)',
                '-webkit-transform': 'translateY(0)',
            });
            $('.fixed-elements').css({
                'top': 0,
                'transition': 'top .3s ease'
            })
            $('.js-show-menu').removeClass('active');
            $('.logo--small.fixed').attr('style', '');
            setTimeout(function() {
                $('.view').css({
                    'transform': 'scale(0.8)',
                    '-ms-transform': 'scale(0.8)',
                    '-webkit-transform': 'scale(0.8)',
                    'opacity': 0,
                });
            }, 500);
            setTimeout(function() {
                $('.view').css({
                    'opacity': 0,
                });
                location.hash = targetRoute;
            }, 1000);

            setTimeout(function() {
                $('.view').css({
                    'transform': 'scale(1)',
                    '-ms-transform': 'scale(1)',
                    '-webkit-transform': 'scale(1)',
                    'opacity': 1,
                });
            }, 1500);
            setTimeout(function() {
                $('.view').attr('style', '');
            }, 1800);
        } else {
            $('.header__right-btn,.header__left-btn').css({
                'transition':'0s'
            })
            if ($('.header__right-btn').hasClass('to-center')){
                $('.header__right-btn').removeClass('.to-center')
                $('.header__left-btn').addClass('.to-center')
            } else if($('.header__left-btn').hasClass('to-center')){
                $('.header__left-btn').removeClass('.to-center')
                $('.header__right-btn').addClass('.to-center')
            }

            var fix = 17;
            switch (targetRoute) {
                case '#bureau':
                    if (isMac) {
                        fix = +5
                    } else {
                        fix = -3
                    }
                    break;
                case '#projects':
                    if (isMac) {
                        fix = +2
                    } else {
                        fix = -2
                    }
                    break;
                case '#contacts':
                    if (isMac) {
                        fix = +8
                    } else {
                        fix = -3
                    }
                    break;
                default:

            }
            var posTar = $(window).outerHeight() / 2 - _this.offset().top - (_this.outerHeight() / 2) - fix;
            posTar = Math.ceil(posTar);
            setTimeout(function() {
                $('.js-menu-bg').unbind('mouseover').unbind('mouseout');



                $('.' + directionImg).css({
                    'opacity': 1,
                })

                $('.bg-wrapper li').css({
                    'transform': 'none',
                    '-ms-transform': 'none',
                    '-webkit-transform': 'none',
                    'transition-delay': 0,
                    'transition': 'none'
                })

                _this.parent().addClass('active');
                _this.addClass('to-center');

                $('.bg-wrapper li:not(.active)').css({
                    'opacity': 0,
                });
                _this.css({
                    'font-size': '90px',
                    'transform': 'translateY(' + posTar + 'px)',
                    '-ms-transform': 'translateY(' + posTar + 'px)',
                    '-webkit-transform': 'translateY(' + posTar + 'px)',
                })
                menuBg.addClass('active');
                $('.js-show-menu').removeClass('active');
                setTimeout(function() {
                    location.hash = targetRoute;
                }, 1200);
                setTimeout(function() {
                    $('.overlay').css({
                        'opacity': '0',
                    })
                    menuMouseEvents();
                }, 1500);
                // debugger;
                //возврощаем меню к прежнему виду
                setTimeout(function() {
                    $('.bg-wrapper li.active').removeClass('active');
                    $('.bg-wrapper li,.bg-wrapper li a').attr('style', '');
                    $('.bg-wrapper li .to-center').removeClass('to-center');
                    $('.bg-wrapper').attr('style', '');
                    menuBg.removeClass('active');
                    $('.overlay').removeClass('active')
                    setTimeout(function() {
                        $('.overlay').css({
                            'opacity': 1
                        });

                        $('.overlay__img').attr('style', '');
                    }, 1000)
                }, 2500)
            }, 800)
        }
    })
});
